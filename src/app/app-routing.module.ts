import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/guards/auth.guard';

const routes: Routes = [
  {
    path:'',
    canActivate: [AuthGuard],
    loadChildren: () => import('./modules/musicapp/musicapp.module').then( m =>  m.MusicappModule ),
  },
  {
    path:'login',
    loadChildren: () => import('./modules/auth/auth.module').then( m =>  m.AuthModule )
  },
  { path: '**', redirectTo: '', pathMatch: 'full' },
  { path: '', redirectTo: '', pathMatch: 'full'  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
