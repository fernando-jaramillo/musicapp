import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpotifyImagePipe } from './spotify-image/sopitify-image.pipe';

@NgModule({
  declarations: [
    SpotifyImagePipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    SpotifyImagePipe
  ]
})
export class PipesModule { }
