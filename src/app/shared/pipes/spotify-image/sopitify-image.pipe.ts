import { Pipe, PipeTransform } from '@angular/core';
import { UserType } from '@interfaces/user.interface';
import { Image } from '@interfaces/spotify.interface';

@Pipe({
  name: 'spotifyImage',
})
export class SpotifyImagePipe implements PipeTransform {
  transform(value?: Image[], ...args: unknown[]): string {
    if (!value) return '';
    const getValue = (img: Image) => (img.height ?? 0) * (img.width ?? 0);

    if (!value.length) {
      if (args.length) {
        const user = args[0] as UserType;
        return `https://ui-avatars.com/api/?name=${user.display_name.replace(
          ' ',
          '+'
        )}`;
      }
      return '';
    }
    return [...value].sort((a, b) => getValue(a) + getValue(b))[0].url;
  }
}
