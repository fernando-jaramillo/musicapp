export interface Pagination<T> {
  href: string;
  items: T[];
  limit: number;
  next: null;
  offset: number;
  previous: null;
  total: number;
}

export interface Image {
  height: number | null;
  url: string;
  width: number | null;
}
