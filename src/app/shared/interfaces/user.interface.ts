import { Image } from './spotify.interface';

export interface UserType {
  country: string;
  display_name: string;
  email: string;
  explicit_content: any;
  external_urls: any;
  followers: Followers;
  href: string;
  id: string;
  images: Image[];
  product: string;
  uri: string;
}

export interface Followers {
  href: null;
  total: number;
}
