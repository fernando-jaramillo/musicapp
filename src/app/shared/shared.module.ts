import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
/* Modules */
import { PipesModule } from './pipes/pipes.module';
/* Components */

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    RouterModule,
    PipesModule,
  ],
  exports: [
  ]
})
export class SharedModule { }
