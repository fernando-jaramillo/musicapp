import { MusicappModule } from './modules/musicapp/musicapp.module';
import { ComponentsModule } from './modules/components/components.module';
import { HttpClientModule } from '@angular/common/http';
import { environment } from '../environments/environment';
/* Core */
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ApiInterceptor } from '@core/interceptors/api.interceptor';
/* Modules */
import { AppRoutingModule } from './app-routing.module';
/* Components */
import { AppComponent } from './app/app.component';
/* NgRx */
import { MetaReducer, StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import state from '@store/app.reducer';
import { AuthService } from '@core/auth/auth.service';
import { AuthEffect } from '@store/auth/auth.effects';
import { UserEffect } from '@store/user/user.effects';
import { TrackEffect } from '@store/spotify/track.effects';
import { localStorageSyncReducer } from './shared/helpers/localstorage-sync.reducer';
import { ReactiveFormsModule } from '@angular/forms';

const metaReducers: Array<MetaReducer<any, any>> = [ localStorageSyncReducer ];

@NgModule({
  declarations: [ AppComponent ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    ComponentsModule,
    MusicappModule,
    ToastrModule.forRoot(),
    StoreModule.forRoot( state, { metaReducers } ),
    EffectsModule.forRoot([ AuthEffect, UserEffect, TrackEffect ]),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
  ],
  providers: [
    AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true,
    },
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
