import { Component,  OnInit, } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '@src/app/core/store/app.reducer';
import { Track } from '@interfaces/track.interface';
import { trackSelectors } from '@store/spotify/track.selector';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {

  public recomendForYou: any = { title: 'Recomendados para tí', data: [] };
  public MostSongYou: any = { title: 'Los más escuchados', data: [] };
  
  constructor( private store: Store<AppState> ) {  }

  ngOnInit(): void {
    this.store.select(trackSelectors.recommendations).subscribe({
      next: (tracks) => { this.setSectionRecomendations(tracks); },
      error: (err) => console.log(err),
    });

    this.store.select(trackSelectors.favorites).subscribe({
      next: (tracks) => { this.setMostSongs(tracks); },
      error: (err) => console.log(err),
    });
  }
  
  setSectionRecomendations(tracks: Track[]) {
    this.recomendForYou = {title: 'Recomendados para tí', data: tracks  };  
  }

  setMostSongs(tracks: Track[]) {
    this.MostSongYou = {title: 'Los más escuchados', data: tracks  };  
  }
}
