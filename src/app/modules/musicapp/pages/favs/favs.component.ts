import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '@src/app/core/store/app.reducer';
import { trackSelectors } from '@src/app/core/store/spotify/track.selector';

@Component({
  selector: 'app-favs',
  templateUrl: './favs.component.html',
  styleUrls: ['./favs.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FavsComponent implements OnInit {
  
  public favsTrackYou: any;

  constructor( private store: Store<AppState>) { }

  ngOnInit(): void {
    this.store.select(trackSelectors.favorites).subscribe({
      next: (tracks) => { this.setFavsComponent(tracks); },
      error: (err) => console.log(err),
    });
  }

  setFavsComponent(tracks: any) {
    this.favsTrackYou = {title: 'Tus canciones favoritas', data: tracks  };  
  }
}
