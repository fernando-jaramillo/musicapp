import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainMusicappComponent } from './main-musicapp.component';

describe('MainMusicappComponent', () => {
  let component: MainMusicappComponent;
  let fixture: ComponentFixture<MainMusicappComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainMusicappComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MainMusicappComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
