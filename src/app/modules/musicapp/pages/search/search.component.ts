import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '@src/app/core/store/app.reducer';
import { trackActions } from '@src/app/core/store/spotify/track.actions';
import { trackSelectors } from '@src/app/core/store/spotify/track.selector';
import { Track } from '@src/app/shared/interfaces/track.interface';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  public queryForm?: any;
  public dataGrid?: any = { title: '',  data: [], };
  public dataGridCurrentyQuery?: any = { title: '',  data: [], };

  constructor( private store: Store<AppState>, private formBuilder: FormBuilder) {
    this.queryForm = this.formBuilder.group({query: '', });
  }

  ngOnInit(): void {
    this.store.select(trackSelectors.searchPageData).subscribe((data) => {
      let labelResult = '';
      if( data.tracks.length > 0 ) { 
        labelResult = `Resultados de búsqueda para: ${ data.currentQuery.toUpperCase() }` 
      }
      this.dataGrid = { title: labelResult, data: data.tracks  };
    });

    this.store.select(trackSelectors.searchPageData).subscribe((data) => {
      this.dataGridCurrentyQuery = { title: 'Ultimás búsquedas: ', data: data.tracks  };
      console.log(this.dataGridCurrentyQuery);
    });


  }

  onSubmit() {
    const query = this.queryForm.getRawValue().query;
    if (!query) return;
    this.store.dispatch(trackActions.getTrackByQuery({ query }));
  }
  
}
