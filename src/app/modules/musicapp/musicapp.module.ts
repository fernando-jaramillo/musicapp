import { NgModule } from '@angular/core';
import { AsyncPipe, CommonModule } from '@angular/common';
/* Routing */
import { MusicappRoutingModule } from './musicapp-routing.module';
/* Modules */
import { CoreModule } from '@core/core.module';
import { ComponentsModule } from '@components/components.module';
import { SharedModule } from '@shared/shared.module';
/* Components */
import { MainMusicappComponent } from './pages/main-musicapp/main-musicapp.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ExplorerComponent } from './pages/explorer/explorer.component';
import { FavsComponent } from './pages/favs/favs.component';
import { SearchComponent } from './pages/search/search.component';
import { SpotifyImagePipe } from '@shared/pipes/spotify-image/sopitify-image.pipe';

@NgModule({
  declarations: [
    MainMusicappComponent,
    DashboardComponent,
    ExplorerComponent,
    FavsComponent,
    SearchComponent,
  ],
  imports: [
    CommonModule,
    MusicappRoutingModule,    
    CoreModule,
    ComponentsModule,
    SharedModule,
  ],
  exports: [ ],
  providers: [
    SpotifyImagePipe,
    AsyncPipe
  ]
})
export class MusicappModule { }
