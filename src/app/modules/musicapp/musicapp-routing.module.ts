import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
/* Components */
import { MainMusicappComponent } from './pages/main-musicapp/main-musicapp.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ExplorerComponent } from './pages/explorer/explorer.component';
import { FavsComponent } from './pages/favs/favs.component';
import { SearchComponent } from './pages/search/search.component';

const routes: Routes = [
  {
    path: '',
    component: MainMusicappComponent,
    children: [      
      {
        path: '',
        component: DashboardComponent
      },
      {
        path: 'explorer',
        component: ExplorerComponent
      },
      {
        path: 'favs',
        component: FavsComponent
      },
      {
        path: 'search',
        component: SearchComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class MusicappRoutingModule { }
