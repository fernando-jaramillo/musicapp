import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { ToastrService } from 'ngx-toastr';
import { filter } from 'rxjs/operators';
/* Store */
import { AppState } from '@core/store/app.reducer';
/* Components */
import { pictureImage } from '@components/atoms/figure-image/figure-image.inteface';
import { authActions } from '@src/app/core/store/auth/auth.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {

  public bg_login!: pictureImage;
  public musicapp_large!: pictureImage;
  public runLoading: boolean = false;
  public loadingSesion: boolean = false

  constructor( 
    private route: Router,
    private toastr: ToastrService,
    private rutaActiva: ActivatedRoute,
    private store: Store<AppState>
  ) { 
    this.bg_login = {
      url: 'assets/images/bg_login.webp',
      alt: 'MusicApp',
      class: 'figure-app'
    };
    this.musicapp_large = {
      url: 'assets/images/musicapp_large.webp',
      alt: 'MusicApp',
      class: 'figure-form'
    };
  }
  
  ngOnInit(): void {  
    this.validateErrorLogin();
    this.store.dispatch(authActions.handleOAuth());
  }


  validateErrorLogin() {
    this.rutaActiva.queryParams
    .pipe( filter( ( params:any )  => params.error ) )
    .subscribe({
      next: ( params: any ) => { 
        this.runLoading = false;
        this.toastr.error('Al parecer hubo un problema al iniciar sesión!', 'Upps!');
        this.store.dispatch(authActions.logout());
        this.route.navigate(['/login']);
      },
      error(err) { console.error('Error: ' + err); }
    })
  }

  loginWithSpotify() : void {
    this.runLoading = true;
    this.store.dispatch(authActions.login());
  }
}
