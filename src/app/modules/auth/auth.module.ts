import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
/* Routing */
import { AuthRoutingModule } from './auth-routing.module';
/* Modules */
import { ComponentsModule } from '@components/components.module';
import { SharedModule } from '@shared/shared.module';
/* Components */
import { MainAuthComponent } from './pages/main-auth/main-auth.component';
import { LoginComponent } from './pages/login/login.component';

@NgModule({
  declarations: [
    MainAuthComponent,
    LoginComponent
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    ComponentsModule,
    SharedModule
  ]
})
export class AuthModule { }
