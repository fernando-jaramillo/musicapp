import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
/* Modules */
import { OrganismsModule } from '@organisms/organisms.module';
/* Components */
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LayoutComponent } from './layout/layout.component';

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    LayoutComponent
  ],
  imports: [
    CommonModule,
    OrganismsModule
  ],
  exports: [
    OrganismsModule,
    HeaderComponent,
    FooterComponent,
    LayoutComponent
  ]
})
export class TemplatesModule { }
