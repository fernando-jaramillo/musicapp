import { Component, Input,  OnInit } from '@angular/core';

@Component({
  selector: 'app-section-slider',
  templateUrl: './section-slider.component.html',
  styleUrls: ['./section-slider.component.scss']
})
export class SectionSliderComponent  {

  @Input() content: any;

  constructor() { }
}
