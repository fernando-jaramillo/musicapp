import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
/* Modules */
import { MoleculesModule } from '@molecules/molecules.module';
import { NavbarComponent } from './navbar/navbar.component';
import { SectionSliderComponent } from './section-slider/section-slider.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { SectionGridComponent } from './section-grid/section-grid.component';
/* Components */

@NgModule({
  declarations: [
    NavbarComponent,
    SectionSliderComponent,
    UserProfileComponent,
    SectionGridComponent
  ],
  imports: [
    CommonModule,
    MoleculesModule
  ],
  exports: [
    MoleculesModule,
    NavbarComponent,
    SectionSliderComponent,
    UserProfileComponent,
    SectionGridComponent
  ]
})
export class OrganismsModule { }
