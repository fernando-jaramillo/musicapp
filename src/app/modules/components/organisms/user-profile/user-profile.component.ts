import { Component, OnInit } from '@angular/core';
import { UserType } from '@interfaces/user.interface';
import { pictureImage } from '@atoms/figure-image/figure-image.inteface';
import { Store } from '@ngrx/store';
import { AppState } from '@src/app/core/store/app.reducer';
import { SpotifyImagePipe } from '@shared/pipes/spotify-image/sopitify-image.pipe';
import { userSelectors } from '@store/user/user.selector';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  public user?: UserType;
  public userPicture?: string;
  public imageUser: pictureImage = { url: '', alt: 'user picture' };

  constructor( private store: Store<AppState>, private spImagePipe: SpotifyImagePipe) { }

  ngOnInit(): void {
    this.store.select(userSelectors.user).subscribe((user) => {
      this.user = user;
      this.userPicture = this.spImagePipe.transform(user?.images);
      this.setImageUser();
    });
  }

  setImageUser(): void {
    this.imageUser = {
      url: this.userPicture ? this.userPicture : '',
      alt: this.user?.display_name ? this.user.display_name : 'user picture',
      class: 'figure-user-profile',
    } 
  }
}
