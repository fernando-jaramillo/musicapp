import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '@core/store/app.reducer';
import { pictureImage } from '@atoms/figure-image/figure-image.inteface';
import { userSelectors } from '@core/store/user/user.selector';
import { UserType } from '@interfaces/user.interface';
import { SpotifyImagePipe } from '@shared/pipes/spotify-image/sopitify-image.pipe';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NavbarComponent implements OnInit {

  public imageBrand: pictureImage;
  public imageUser: pictureImage;
  public menuUSer:boolean = false;
  public user?: UserType;
  public userPicture! : string;
  
  constructor( private store: Store<AppState>, private spImagePipe: SpotifyImagePipe ) {
    this.imageBrand = {
      url: 'assets/images/musicapp_large.webp',
      alt: 'MusicApp',
      class: 'figure-brand',
      width: '100%',
      height: '100%'
    }
    this.imageUser = {
      url: 'assets/images/musicapp.webp',
      alt: 'Logo',
      class: 'figure-user'
    } 
  }

  ngOnInit(): void {
    this.store.select(userSelectors.user)
    .subscribe((user) => {
      this.user = user;
      this.userPicture = this.spImagePipe.transform(user?.images);
      this.setImageUser();
    });
  }

  setImageUser(): void {
    this.imageUser = {
      url: this.userPicture ? this.userPicture : '',
      alt: this.user?.display_name ? this.user.display_name : 'user picture',
      class: 'figure-user',
    } 
  }

  showMenuUSer(): void {
    this.menuUSer = !this.menuUSer;
  }
}
