import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-section-grid',
  templateUrl: './section-grid.component.html',
  styleUrls: ['./section-grid.component.scss']
})
export class SectionGridComponent implements OnInit {

  @Input() content: any;
  
  constructor() { }

  ngOnInit(): void {
  }

}
