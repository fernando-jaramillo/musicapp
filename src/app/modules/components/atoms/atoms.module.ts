import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
/* Modules */
/* Components */
import { FigureImageComponent } from './figure-image/figure-image.component';
import { BorderAnimatedComponent } from './border-animated/border-animated.component';
import { InputSearchComponent } from './input-search/input-search.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    FigureImageComponent,
    BorderAnimatedComponent,
    InputSearchComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    FigureImageComponent,
    BorderAnimatedComponent,
    InputSearchComponent
  ]
})
export class AtomsModule { }
