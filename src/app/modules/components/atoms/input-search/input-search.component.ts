import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-input-search',
  templateUrl: './input-search.component.html',
  styleUrls: ['./input-search.component.scss']
})
export class InputSearchComponent {

  @Input() form!: FormGroup<{query: FormControl<string | null>;}>;
  @Output('onSubmit') onSubmitEvent = new EventEmitter();

  constructor() {}
  onSubmit() { this.onSubmitEvent.emit(); }
}
