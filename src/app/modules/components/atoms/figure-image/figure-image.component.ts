import { Component, Input, OnInit } from '@angular/core';
import { pictureImage } from './figure-image.inteface';

@Component({
  selector: 'app-figure-image',
  templateUrl: './figure-image.component.html',
  styleUrls: ['./figure-image.component.scss']
})
export class FigureImageComponent implements OnInit {

  @Input() image!: pictureImage;
  
  constructor() {
    this.image = { 
      url:    'assets/images/404.png', 
      alt:    'MusicApp', 
      class:  'figure-app',
      width:  '100%',
      height: '100%'
    };
  }
  ngOnInit(): void {
    if( !this.image.url ) { this.image.url = 'assets/images/404.png'; }
  }

}
