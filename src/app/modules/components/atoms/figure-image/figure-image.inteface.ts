export interface pictureImage {
  url:     string;
  alt:     string;
  class?:  string;
  width?:  string;
  height?: string;
}