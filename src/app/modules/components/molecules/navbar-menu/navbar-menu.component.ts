import { Component } from '@angular/core';

@Component({
  selector: 'app-navbar-menu',
  templateUrl: './navbar-menu.component.html',
  styleUrls: ['./navbar-menu.component.scss']
})
export class NavbarMenuComponent  {

  public primaryMenu: any[] = [];

  constructor() { 
    this.primaryMenu = [
      {
        router: '/',
        label: 'Principal',
        icon: 'bx bxs-home-alt-2'
      },
      {
        router: '/explorer',
        label: 'Explorar',
        icon: 'bx bx-compass'
      },
      {
        router: '/favs',
        label: 'Favoritos',
        icon: 'bx bxs-star'
      },
      {
        router: '/search',
        label: 'Buscar',
        icon: 'bx bx-search-alt-2'
      }
    ]

  }

}
