import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '@src/app/core/store/app.reducer';
import { authActions } from '@src/app/core/store/auth/auth.actions';
import { userSelectors } from '@src/app/core/store/user/user.selector';
import { UserType } from '@src/app/shared/interfaces/user.interface';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss']
})
export class UserMenuComponent {

  public user$?: Observable<UserType | undefined>;

  constructor( private store: Store<AppState>, private route: Router ) { }
  
  ngOnInit(): void {
    this.user$ = this.store.select(userSelectors.user);
  }

  logout() {
    this.store.dispatch(authActions.logout());
    this.route.navigate(['/login']);
  }
}
