import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-splide-slide',
  templateUrl: './splide-slide.component.html',
  styleUrls: ['./splide-slide.component.scss']
})
export class SplideSlideComponent   {

  @Input() dataSlider: any[] = [];
  public  optionSlide: any = {};

  constructor() {
    this.optionSlide = {
      type: 'loop', 
      perPage: 5, 
      keyboard: false, 
      pauseOnFocus: true, 
      autoplay: false, 
      arrows: false,
      updateOnMove: true,
      interval: 4500,
      breakpoints: {
        520: {
          perPage: 2,
        },
        992: {
          perPage: 3,
        },
        1200: {
          perPage: 4,
        },
      }
    }
  }
}
