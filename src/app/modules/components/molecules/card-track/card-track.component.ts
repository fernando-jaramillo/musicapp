import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { pictureImage } from '@atoms/figure-image/figure-image.inteface';
import { Track } from '@interfaces/track.interface';
import { Store } from '@ngrx/store';
import { SpotifyImagePipe } from '@src/app/shared/pipes/spotify-image/sopitify-image.pipe';
import { AppState } from '@store/app.reducer';
import { trackActions } from '@store/spotify/track.actions';
import { trackSelectors } from '@store/spotify/track.selector';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-card-track',
  templateUrl: './card-track.component.html',
  styleUrls: ['./card-track.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CardTrackComponent implements OnInit {

  @Input() track?: Track;
  isFavorite: boolean = false;
  
  constructor( 
    private store: Store<AppState>, 
    private spImagePipe: SpotifyImagePipe, 
    private toastr: ToastrService ) 
  { }

  ngOnInit(): void {
    if (this.track) {
      this.store
        .select(trackSelectors.isFavorite(this.track.id))
        .subscribe((isFavorite) => {
          this.isFavorite = isFavorite;
        });
    }
  }

  getArtistNames() {
    if (!this.track) return '';
    return this.track.artists.map((artist) => artist.name).join(', ') ?? '';
  }

  getImageTrack(): pictureImage {
    if (!this.track) return {
      url: 'https://picsum.photos/200/200', 
      alt: 'Picture Lorem', 
      class: 'picture-card' 
    };
    return { 
      url: this.spImagePipe.transform(this.track.album.images),
      alt: this.track.name,
      class: 'picture-card' 
    }
  }

  onFavoriteClick() {
    if (!this.track) return;
    if (this.isFavorite) {
      this.store.dispatch(trackActions.removeFavorite({ track: this.track }));
      this.toastr.error('Removido de favoritos');
    }
    else {
      this.store.dispatch(trackActions.addFavorite({ track: this.track }));
      this.toastr.success('Agregado a favoritos');
    }
      
  }
}
