import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgxSplideModule } from 'ngx-splide';
/* Modules */
import { AtomsModule } from '@atoms/atoms.module';
/* Components */
import { NavbarMenuComponent } from './navbar-menu/navbar-menu.component';
import { UserMenuComponent } from './user-menu/user-menu.component';
import { CardTrackComponent } from './card-track/card-track.component';
import { SplideSlideComponent } from './splide-slide/splide-slide.component';
import { GridCardsComponent } from './grid-cards/grid-cards.component';

@NgModule({
  declarations: [
    NavbarMenuComponent,
    UserMenuComponent,
    CardTrackComponent,
    SplideSlideComponent,
    GridCardsComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    AtomsModule,
    NgxSplideModule
  ],
  exports: [
    AtomsModule,
    RouterModule,
    NavbarMenuComponent,
    UserMenuComponent,
    CardTrackComponent,
    SplideSlideComponent,
    GridCardsComponent
  ]
})
export class MoleculesModule { }
