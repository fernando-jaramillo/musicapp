import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-grid-cards',
  templateUrl: './grid-cards.component.html',
  styleUrls: ['./grid-cards.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class GridCardsComponent implements OnInit {

  @Input() dataGrid: any[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
