import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../core/store/app.reducer';
import { userActions } from '../core/store/user/user.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: []
})
export class AppComponent {
  title = 'musicApp';

  constructor(private store: Store<AppState>) {}
  
  ngOnInit(): void {
    this.store.dispatch(userActions.getUser());
  }
  
}
