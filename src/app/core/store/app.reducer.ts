import { ActionReducer, Action, Store } from '@ngrx/store';
import { authReducer } from './auth/auth.reducer';
import { trackReducer } from './spotify/track.reducer';
import { userReducer } from './user/user.reducer';

const state = {
  auth:   authReducer,
  user:   userReducer,
  track:  trackReducer,
};

type tempStateType = typeof state;

export type AppState = {
  [K in keyof tempStateType]: tempStateType[K] extends ActionReducer< infer O, Action  > ? O : never;
};

export default state;