import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { EMPTY } from 'rxjs';

import { userActions } from '../user/user.actions';
import { trackActions } from './track.actions';
import { SpotifyService } from '@services/spotify-service/spotify.service';

@Injectable()
export class TrackEffect {
  constructor(
    private actions$: Actions,
    private trackService: SpotifyService
  ) {}

  getRecomendations$ = createEffect(() =>
    this.actions$.pipe(
      ofType(userActions.getUserSuccess),
      mergeMap(() =>
        this.trackService.getUserRecomendations().pipe(
          map((recommendations) =>
            trackActions.getRecomendationsSuccess(recommendations)
          ),
          catchError(() => EMPTY)
        )
      )
    )
  );

  getTrackByQuery$ = createEffect(() =>
    this.actions$.pipe(
      ofType(trackActions.getTrackByQuery),
      mergeMap((actions) =>
        this.trackService.getTracksByQuery(actions.query).pipe(
          map(({ tracks }) =>
            trackActions.getTrackByQuerySuccess({ tracks: tracks.items })
          ),
          catchError(() => EMPTY)
        )
      )
    )
  );
}
