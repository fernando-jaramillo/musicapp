import { createSelector } from '@ngrx/store';
import { AppState } from '../app.reducer';

const selectTrackState = (state: AppState) => state.track;

const recommendations = createSelector(
  selectTrackState,
  (track) => track.recommendations
);

const favorites = createSelector(selectTrackState, (track) => track.favorites);

const isFavorite = (id: string) =>
  createSelector(
    selectTrackState,
    (track) => !!track.favorites.find((track) => track.id === id)
  );

const searchPageData = createSelector(selectTrackState, (track) => ({
  currentQuery: track.currentQuery,
  tracks: track.searchTracks,
}));

export const trackSelectors = {
  recommendations,
  favorites,
  isFavorite,
  searchPageData,
};
