import { createAction, props } from '@ngrx/store';
import { Recommendations, Track } from '@interfaces/track.interface';


const getRecomendationsSuccess = createAction(
  '[Track] Get Recommendations Success',
  props<Recommendations>()
);

const addFavorite = createAction(
  '[Track] Add Favorite',
  props<{ track: Track }>()
);

const removeFavorite = createAction(
  '[Track] Add Remove',
  props<{ track: Track }>()
);

const getTrackByQuery = createAction(
  '[Track] Get Track By Query',
  props<{ query: string }>()
);

const getTrackByQuerySuccess = createAction(
  '[Track] Get Track By Query Success',
  props<{ tracks: Track[] }>()
);

export const trackActions = {
  getRecomendationsSuccess,
  addFavorite,
  removeFavorite,
  getTrackByQuery,
  getTrackByQuerySuccess,
};
