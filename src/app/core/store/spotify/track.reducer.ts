import { createReducer, on } from '@ngrx/store';
import { Track } from '@interfaces/track.interface';
import { trackActions } from './track.actions';

export interface TrackStateType {
  recommendations: Track[];
  favorites: Track[];
  currentQuery: string;
  searchTracks: Track[];
}

export const initialState: TrackStateType = {
  recommendations: [],
  favorites: [],
  currentQuery: '',
  searchTracks: [],
};

export const trackReducer = createReducer(
  initialState,
  on(trackActions.getRecomendationsSuccess, (state, payload) => ({
    ...state,
    recommendations: payload.tracks,
  })),
  on(trackActions.addFavorite, (state, payload) => ({
    ...state,
    favorites: [...state.favorites, payload.track],
  })),
  on(trackActions.removeFavorite, (state, payload) => ({
    ...state,
    favorites: state.favorites.filter((track) => track.id != payload.track.id),
  })),
  on(trackActions.getTrackByQuery, (state, payload) => ({
    ...state,
    currentQuery: payload.query,
  })),
  on(trackActions.getTrackByQuerySuccess, (state, payload) => ({
    ...state,
    searchTracks: payload.tracks,
  }))
);
