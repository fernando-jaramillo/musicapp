import { createAction, props } from '@ngrx/store';
import { AuthStateType } from './auth.reducer';

const login        = createAction('[Auth] Login');
const logout       = createAction('[Auth] Logout');
const handleOAuth  = createAction('[Auth] Handle OAuth');
const loginSuccess = createAction('[Auth] Login Success', props<AuthStateType['tokens']>() );

export const authActions = {
  login,
  loginSuccess,
  handleOAuth,
  logout,
};