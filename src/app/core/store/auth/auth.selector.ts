import { createSelector } from '@ngrx/store';
import { AppState } from '../app.reducer';


const selectAuthState = (state: AppState) => state.auth;

const isLogged = createSelector(selectAuthState, (auth) => auth.isLogged);

const accessToken = createSelector(
  selectAuthState,
  (auth) => auth.tokens.accessToken
);

export const authSelectors = {
  isLogged,
  accessToken,
};
