import { createReducer, on } from '@ngrx/store';
import { authActions } from './auth.actions';

export interface AuthStateType {
  isLogged: boolean;
  tokens: {
    accessToken?: string;
  };
}
// Initial state of Auth Reducer
export const initialState: AuthStateType = {
  isLogged: false,
  tokens: {},
};

export const authReducer = createReducer(
  initialState,
  on(authActions.loginSuccess, (_, { type, ...tokens }) => ({
    isLogged: true,
    tokens,
  })),
  on(authActions.logout, () => ({
    isLogged: false,
    tokens: {},
  }))
);