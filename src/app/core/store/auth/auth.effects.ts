import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import { authActions } from './auth.actions';
import { EMPTY, lastValueFrom, of } from 'rxjs';
import { Store } from '@ngrx/store';
import { authSelectors } from './auth.selector';
import { AppState } from '../app.reducer';
import { AuthService } from '../../auth/auth.service';

@Injectable()
export class AuthEffect {
  login$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(authActions.login),
        tap(this.authService.oAuthInit)
      ),
    {
      dispatch: false,
    }
  );

  handleOAuth$ = createEffect(() =>
    this.actions$.pipe(
      ofType(authActions.handleOAuth),
      concatLatestFrom(() => this.store.select(authSelectors.isLogged)),
      mergeMap(([_, isLoggedIn]) => {
        if (isLoggedIn) { this.router.navigate(['']); return EMPTY; }
        if (!this.route.snapshot.fragment) return EMPTY;
        const accessToken = this.route.snapshot.fragment?.split('=')[1].split('&')[0];
        this.router.navigate(['']);
        return of(
          authActions.loginSuccess({
            accessToken,
          })
        );
      })
    )
  );

  constructor(
    private store: Store<AppState>,
    private actions$: Actions,
    private route: ActivatedRoute,
    private authService: AuthService,
    private router: Router
  ) {}
}
