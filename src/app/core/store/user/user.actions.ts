import { createAction, props } from '@ngrx/store';
import { UserType } from '@src/app/shared/interfaces/user.interface';

const getUser = createAction('[User] Get User');

const getUserSuccess = createAction(
  '[User] Get User Success',
  props<UserType>()
);

const clearUser = createAction('[User] clear user');

export const userActions = {
  getUser,
  getUserSuccess,
  clearUser,
};
