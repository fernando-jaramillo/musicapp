import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap, tap } from 'rxjs/operators';
import { EMPTY, of } from 'rxjs';
import { UserService } from '@services/user-service/user.service';
import { userActions } from './user.actions';
import { authActions } from '../auth/auth.actions';

@Injectable()
export class UserEffect {

  
  getUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(userActions.getUser, authActions.loginSuccess),
      mergeMap(() =>
        this.userService.getUser().pipe(
          map( (user) => userActions.getUserSuccess(user)),
          catchError(() => EMPTY)
        )
      )
    )
  );

  clearUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(authActions.logout),
      mergeMap(() => of(userActions.clearUser()))
    )
  );

  constructor(private actions$: Actions, private userService: UserService) {}
}
