import { createReducer, on } from '@ngrx/store';
import { UserType } from '@src/app/shared/interfaces/user.interface';
import { userActions } from './user.actions';

export interface UserStateType {
  user?: UserType;
}

export const initialState: UserStateType = {};

export const userReducer = createReducer(
  initialState,
  on(userActions.getUserSuccess, (state, { type, ...user }) => ({
    ...state,
    user,
  })),
  on(userActions.clearUser, (state, { type, ...user }) => ({
    ...state,
    user: undefined,
  }))
);
