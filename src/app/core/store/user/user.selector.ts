import { createSelector } from '@ngrx/store';
import { AppState } from '../app.reducer';

const selectUserState = (state: AppState) => state.user;

const user = createSelector(selectUserState, (user) => user.user);

export const userSelectors = { user };
