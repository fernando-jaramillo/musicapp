import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { environment } from '@src/environments/environment.prod';
import { AppState } from '../store/app.reducer';
import { authSelectors } from '../store/auth/auth.selector';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor( private store: Store<AppState> ) { }

  oAuthInit() {
    const authEndpoint = `${environment.SP_AUTH_ENDPOINT}?`;
    const authClientId = `client_id=${environment.SP_CLIENT_ID}&`;
    const authRedirectUrl = `redirect_uri=${environment.SP_REDIRECT_URI}&`;
    const authScopes = `scope=${environment.SP_SCOPES.join('%20')}&`;
    const responseType = `response_type=token&show_dialog=true`;
    const AUTH_URL = authEndpoint + authClientId + authRedirectUrl + authScopes + responseType;
    location.replace( AUTH_URL ) ;
  }
  
  isLoggedIn() {
    return this.store.select(authSelectors.isLogged);
  }
}
