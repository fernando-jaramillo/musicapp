import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpContextToken,
  HttpErrorResponse,
} from '@angular/common/http';
import { catchError, map, Observable, of, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { AppState } from '../store/app.reducer';
import { authSelectors } from '../store/auth/auth.selector';
import { authActions } from '../store/auth/auth.actions';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
  accessToken: string | undefined;
  constructor(private store: Store<AppState>, private router: Router) {
    this.store
      .select(authSelectors.accessToken)
      .subscribe((token) => (this.accessToken = token));
  }

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    if (!this.accessToken) {
      throw new HttpErrorResponse({
        error: 'not access-token',
        status: 500,
        statusText: 'error',
      });
    }

    const apiReq = request.clone({
      url: `${environment.SP_SPOTIFY_API}${request.url}`,
      headers: request.headers.set(
        'Authorization',
        `Bearer ${this.accessToken}`
      ),
    });

    return next.handle(apiReq).pipe(
      catchError((err: HttpErrorResponse) => {
        if (err?.error?.error?.message === 'The access token expired') {
          this.store.dispatch(authActions.logout());
          this.router.navigate(['/login']);
        }
        return of();
      })
    );
  }
}
