import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
/* Modules */
import { ComponentsModule } from '@components/components.module';
import { SharedModule } from '@shared/shared.module';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    SharedModule,
    HttpClientModule
  ],
  exports: [
  ]
})
export class CoreModule { }
