import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserType } from '@interfaces/user.interface';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  getUser() {
    return this.http.get<UserType>('/me');
  }
}
