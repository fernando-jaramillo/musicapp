import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { mergeMap } from 'rxjs';

import { RecentlyPlayed, Recommendations, Track } from '@interfaces/track.interface';
import { Pagination } from '@interfaces/spotify.interface';


@Injectable({
  providedIn: 'root'
})
export class SpotifyService {
  constructor(private http: HttpClient) {}

  getUserRecentlyPlayed() {
    return this.http.get<Pagination<RecentlyPlayed>>(
      '/me/player/recently-played?after=5&limit=50'
    );
  }

  getUserRecomendations() {
    return this.getUserRecentlyPlayed().pipe(
      mergeMap(({ items }) => {
        if (!items.length)
          return this._getUserRecomendations('seed_genres=salsa');

        return this._getUserRecomendations(
          `seed_artists=${items
            .slice(0, 4)
            .map((track) => {
              if (!track.track.artists.length) return null;
              return track.track.artists[0].id;
            })
            .filter((artist) => artist)
            .join(',')}`
        );
      })
    );
  }

  _getUserRecomendations(filter: string) {
    return this.http.get<Recommendations>(`/recommendations?${filter}`);
  }

  getTracksByQuery(query: string) {
    return this.http.get<{ tracks: Pagination<Track> }>('/search', {
      params: {
        query,
        type: 'track',
      },
    });
  }
}
