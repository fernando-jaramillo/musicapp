// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production:       false,
  SP_CLIENT_ID:     '44eb4bd54201418e98056f102209bad5',
  SP_AUTH_ENDPOINT: 'https://accounts.spotify.com/authorize',
  SP_REDIRECT_URI:  'http://localhost:4200/login',
  SP_SPOTIFY_API:   'https://api.spotify.com/v1',
  SP_SCOPES : [
    'user-read-currently-playing', // Musica tocando ahora
    'user-read-recently-played',  // Musica recientemente escuchada
    'user-read-playback-state', // Estado de la musica
    'user-read-private', // Informacion del usuario'
    'user-read-email', // Email del usuario
    'user-top-read', // Top artistas y musica del usuario
    'user-modify-playback-state', // Modificar la musica del usuario
    'user-library-read', // Leer la musica del usuario
    'user-library-modify', // Modificar la musica del usuario
    'playlist-read-private', // Leer las playlist del usuario
    'playlist-read-collaborative' // Leer las playlist colaborativas del usuario
  ]
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
